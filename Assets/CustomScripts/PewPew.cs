﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PewPew : MonoBehaviour {

	// Use this for initialization
    public float moveSpeed;
    public GameObject player;
    Vector3 playerPos;
    Vector3 delta;

    void Start () {
        player = GameObject.Find("Main Camera");
        playerPos = player.transform.position;
        delta = player.transform.position - transform.position;
        delta.Normalize();

        GetComponent<Rigidbody>().AddForce(delta * moveSpeed);
    }
	
	// Update is called once per frame
	void Update ()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject == player)
        {
            Text livePoint = (Text)GameObject.Find("LivePoint").GetComponent<Text>();
            int lp = Convert.ToInt16(livePoint.text); 
            if(lp > 0)
            {
                livePoint.text = (lp - 1).ToString();
            }
        }
    }
}
