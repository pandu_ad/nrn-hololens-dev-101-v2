﻿#define UNITY_EDITOR_WIN

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratePew : MonoBehaviour {

    public int interval = 5;
    public Object prefab;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("Spawn", 0f, interval);
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    //Spawn
    void Spawn()
    {
        GameObject clone = Instantiate(prefab, new Vector3(0, 1, 7), Quaternion.identity) as GameObject;
    }
}
